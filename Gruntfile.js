module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
      
      less: {
          development: {
              options: {
                  compress: true,
                  yuicompress: true,
                  optimization: 2
              },
              files: {
                  "public/css/style.css": "assets/css/src/style.less"
              }
          }
      },

      watch: {
          stylesheets: {
              files: ["assets/css/**/*.less"],
              task: ["less"],
              options: {
                  nospawn: true
              }
          }
      }
  });

  grunt.registerTask("default", ["less", "watch"]);
  grunt.registerTask('dist', ['concat:dist', 'uglify:dist']);
};
