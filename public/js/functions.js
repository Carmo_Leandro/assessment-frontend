/*
Teste Frontend - WebJump

Leandro Pereira da Silva do Carmo
 */

$(document).ready(function() {

    // Ao carregar a página faz o tratamento para menu mobile

    $('.toggleMenu').click(function() {
        $('#top-menu').slideToggle('slow');
    });

    $('.btn-buy').on('click', function() {
       $(this).attr
    });

    // Chama a função de renderização da página
    renderPage();
});

/*
O array de cores foi definido fora das funções devido ao escopo e
para evitar duplicidade quando carregado todos os produtos.
 */

var filters = [];

// Renderiza a página
function renderPage() {

    // Chama a função que renderiza o menu conforme tamanho do dispositivo
    renderMenu();

    // Chama a função que define o path para carregar elementos conforme itens selecionados
    getPath();

    updateBreadcrumbs();
}

// Renderiza o menu conforme dispositivo

function renderMenu() {
    $('#mmenu').append('<a href="/index.html"><li>Página inicial</li></a>');
    $.getJSON('/api/V1/categories/list', function(list_categories) {
        const links = list_categories.items;
        $.each(links, function(menu_item) {
            $('#mmenu').append('<a href="/category.html?cat=' + links[menu_item].id + '"><li>' + links[menu_item].name + '</li></a>');
        });
        $('#mmenu').append('<a href="/contact.html"><li>Contato</li></a>');
    });
}

// Define o path para carregar os itens

function getPath() {
    // Verifica a categoria e filtros selecionados por parametro

    let queryString = window.location.search;
    let urlParams = new URLSearchParams(queryString);
    let cat = urlParams.get('cat');
    let category = cat === null ? loadAllProducts() : listByCategory(cat);
    return category;
}

// Caso não haja nenhum path definido faz o load de todas as categorias

function loadAllProducts() {
    $('#products').empty();
    $.getJSON('/api/V1/categories/list', function(list_categories) {
        let items = list_categories.items;
        let i = 0;
        $.each(items, function() {
            listByCategory(items[i].id);
            i++
        });
        $('#categoria').html('Todas as Categorias');
    });
}

// Lista os produtos do catálogo conforme path

function listByCategory(category) {
    $.getJSON('/api/V1/categories/' + category, function(list_products) {
        let i = 0;
        let items = list_products.items;

        // Recebe os valores dos atributos dos produtos

        $.each(items, function(list) {
            let id = items[i].id;
            let sku = items[i].sku;
            let path = items[i].path;
            let name = items[i].name;
            let image = items[i].image;
            let price = items[i].price;
            let specialPrice = items[i].specialPrice;
            let filter = items[i].filter[0].color;
            let gender = items[i].filter[0].gender;

            /*
            Antes de fazer o push no array de cores é feito duas verificações
            1 - Se o produto não possui filtro por cor ele resultaria em um
                valor undefined.
            2 - Se o filtro já foi adicionado no array anteriormente.

            Atendidas essas duas condições o push no array é realizado.
             */

            if(filter !== undefined && $.inArray(filter, filters) === -1) {
                filters.push(filter);
                $('#cores').append('<li class="' + filter +'"><a href="?filter=' + filter + '"><img onclick="applyColor(event)" apply="' + filter + '" src="media/filters/' + filter + '.png' + '"></a></li>');
            }

            // Realiza o tratamento de produtos com promoção

            let currentPrice = specialPrice ? specialPrice : price;
            currentPrice = currentPrice.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
            price = price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
            if(specialPrice == undefined) {
                $('.old-price').hide();
            }

            // Renderiza os blocos de produtos na tela

            // Verifica filtros ativos antes de renderizar os produtos

            let product = '<div id="product-'+ id +'" class="item">' +
                '<div class="img">' +
                '<img src="' + image + '" alt="' + name + '">' +
                '</div>' +
                '<div class="name">' + name +
                '<div class="old-price">' + price +
                '</div>' +
                '<div class="price">' + currentPrice +
                '</div>' +
                '<div class="buy">' +
                '<button type="button" onclick="addToCart(event)" id=" ' + id + ' " price="' + price + '" image="' + image + '" class="btn-buy" name="' + name + '">Comprar</button>'  +
                '</div>' +
                '</div>';

            $('#products').append(product);
            if (gender){
                $('#product-' + id).attr('gender', gender);
            }
            if (filter){
                $('#product-' + id).attr('color', filter);
            }
            i++
        });
    });
}

// Exibe o modal de adição no carrinho, simulando uma compra

function addToCart() {

    // Cria o popup e os dados do produto selecionado

    var name = event.target.attributes.name.value;
    var image = event.target.attributes.image.value;
    var price = event.target.attributes.price.value;

    $('#namePopup').html(name);
    $('#imgPopup').prop('src', image);
    $('#pricePopup').html(price);

    $('.addToCart, .windowCart, .addCartWindow').toggleClass('active');
    $('.addToCart, .windowCart, .addCartWindow').on('click', function() {
        $('.addToCart, .windowCart, .addCartWindow').removeClass('active');
    });
}

/*
Recebe pelo evento de clique o filtro desejado, uma vez definido o filtro
realiza a remoção dos itens diferentes à seleção.
 */

function applyColor() {
    event.preventDefault();
    let color = event.target.attributes.apply.value;
    $('.item').show();
    $('.item').filter("[color!='" + color + "']").hide();
}

function showFilter(f) {
    $('.categories').hide();
    $('.filter-' + f).toggle();
}

function updateBreadcrumbs() {
    // Faz a atualização do nome da categoria no breadcrumbs

    let queryString = window.location.search;
    let urlParams = new URLSearchParams(queryString);
    let cat = urlParams.get('cat');

    if (cat === null) {
        $('#categoria').html('Todas as Categorias');
    } else {
        $.getJSON('/api/V1/categories/list', function(catTitle) {
            cat = cat -1;
            $('#categoria').html(catTitle.items[cat].name);
        });
    }
}

function gridList() {
    let listing = event.target.attributes.id.value;

    console.log(listing);

    if(listing === 'list') {
        $('#products, .item').addClass('list');
        $('#list').addClass('active');
        $('#grid').removeClass('active');

    } else if(listing === 'grid') {
        $('#products, .item').removeClass('list');
        $('#list').removeClass('active');
        $('#grid').addClass('active');
    }
}